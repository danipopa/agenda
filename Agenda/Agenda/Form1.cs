﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    public partial class agendaForm : Form
    {
        public agendaForm()
        {
            InitializeComponent();
        }
       
        private void agendaForm_Load(object sender, EventArgs e)
        {
            AgendaDB agenda = new AgendaDB();
            personGrid.DataSource = agenda.personSelect();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AgendaDB agenda = new AgendaDB();
            agenda.addPerson(nameTextBox.Text,phoneTextBox.Text,emailTextBox.Text);
            personGrid.DataSource = agenda.personSelect();
            int id = personGrid.Rows.Count;
            if (id >1)
            {
                personGrid.Rows[id - 1].Selected = true;
                personGrid.Rows[0].Selected = false;
            }

        }
        public DataGridViewRow personGridSelectedRow()
        {

            DataGridViewRow selectedRow = personGrid.Rows[personGrid.SelectedRows[0].Index];
            return selectedRow;

        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
           
            try
            {
                AgendaDB agenda = new AgendaDB();
                DataGridViewRow selectedRow = personGridSelectedRow();
                agenda.deletePerson(selectedRow.Cells[0].Value.ToString(), selectedRow.Cells[1].Value.ToString(), selectedRow.Cells[2].Value.ToString());
                personGrid.DataSource = agenda.personSelect();
               
              
            }
            catch(Exception ex) { MessageBox.Show("ERROR " + ex.Message); }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                AgendaDB agenda = new AgendaDB();
                DataGridViewRow selectedRow = personGridSelectedRow();
                int id = personGrid.SelectedRows[0].Index;
                agenda.updatePerson(selectedRow.Cells[0].Value.ToString(), selectedRow.Cells[1].Value.ToString(), selectedRow.Cells[2].Value.ToString(), nameTextBox.Text, phoneTextBox.Text, emailTextBox.Text);
                personGrid.DataSource = agenda.personSelect();
                personGrid.Rows[id].Selected = true;
                personGrid.Rows[0].Selected = false;
            }
           catch (Exception ex) { MessageBox.Show("ERROR " + ex.Message); }


        }

        private void applyFilterButton_Click(object sender, EventArgs e)
        {
            AgendaDB agenda = new AgendaDB();
            personGrid.DataSource=agenda.applyFilter(filterTextBox.Text);
        }

    }
}
