﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace Agenda
{
    class AgendaDB
    {
        List<Person> personList = new List<Person>();
        private SqlConnection connectionDB()
        {
             SqlConnection connection = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=Informatii;Integrated Security=True");
            return connection;
             
        }
        public List<Person> personSelect()
        {
          
            SqlConnection conn = connectionDB();
           
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT *FROM Person", conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Person person = new Person();
                    person.Name = reader["Name"].ToString();
                    person.Phone = reader["Phone"].ToString();
                    person.Email = reader["Email"].ToString();

                    personList.Add(person);
                    
                }
               
            }
            catch (Exception ex) { MessageBox.Show("Error: "+ex.Message); }

        return personList;
        }
        public void addPerson(String name,String phone,String email)
        {
            SqlConnection conn = connectionDB();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO Person (Name,Phone,Email) VALUES (@name,@phone,@email)",conn);
                cmd.Parameters.AddWithValue("@name",name);
                cmd.Parameters.AddWithValue("@phone",phone);
                cmd.Parameters.AddWithValue("@email",email);
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex) { MessageBox.Show("Error: " + ex.Message); }
        }
   
        public void deletePerson(String name,String phone,String email)
        {
            SqlConnection conn = connectionDB();
           
            try
            {
                
                conn.Open();
                SqlCommand cmd = new SqlCommand("DELETE FROM Person Where Name=@name AND Phone=@phone AND Email=@email", conn);
                cmd.Parameters.AddWithValue("@name",name);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();

            }
            catch(Exception ex) { MessageBox.Show("ERROR: "+ex.Message); }

        }
        public void updatePerson(String name,String phone,String email,String newName,String newPhone,String newEmail)
        {
            SqlConnection conn = connectionDB();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("UPDATE Person SET Name=@newName,Phone=@newPhone,Email=@newEmail WHERE Name=@name AND Phone=@phone AND Email=@email",conn);
                cmd.Parameters.AddWithValue("@newName",newName);
                cmd.Parameters.AddWithValue("@newPhone",newPhone);
                cmd.Parameters.AddWithValue("@newEmail",newEmail);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@email", email);

                cmd.ExecuteNonQuery();

            }
            catch(Exception ex) { MessageBox.Show("ERROR: "+ex.Message); }
        }
        public List<Person> applyFilter(String name)
        {
            SqlConnection conn = connectionDB();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT Name,Phone,Email FROM Person WHERE Name LIKE @name",conn);
                cmd.Parameters.AddWithValue("@name",name+"%");
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    Person p = new Person();
                    p.Name = reader["Name"].ToString();
                    p.Phone = reader["Phone"].ToString();
                    p.Email = reader["Email"].ToString();

                    personList.Add(p);
                }
            }
            catch(Exception ex) { MessageBox.Show("ERROR " + ex.Message); }
            return personList;
        }
    }
}
