﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda
{
    class Person
    {
        private String name { get; set; }
        private String phone { get; set; }
        private String email { get; set; }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        public String Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        public String Email
        {
            get { return email; }
            set { email = value; }
        }
    }
}
